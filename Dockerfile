FROM yannoff/maven:3.8.5-openjdk-17-alpine
WORKDIR /app
COPY . /app
RUN mvn dependency:go-offline
RUN mvn package -DskipTests
EXPOSE 8080

CMD ["java", "-jar", "/app/target/ghd.jar"]
